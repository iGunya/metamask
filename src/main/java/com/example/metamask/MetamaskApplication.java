package com.example.metamask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetamaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetamaskApplication.class, args);
	}

}
