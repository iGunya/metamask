package com.example.metamask.controller;

import com.example.metamask.service.WebDriverSend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class MetaController {
    @Autowired
    private WebDriverSend webDriverSend;
    
    private String[] content;
    private List<String> wordsArr = new ArrayList<>();
    private boolean[] chose;

    @GetMapping("/start")
    public String start(){
        return "start";
    }

    @PostMapping("/start")
    public void startM(@RequestPart(value= "fileName") final MultipartFile multipartFile) throws IOException {
        content = new String(multipartFile.getBytes(), StandardCharsets.UTF_8).split(" |\r\n");
        chose = new boolean[content.length];
        search();
    }

    private File convertMultiPartToFile(MultipartFile file ) throws IOException
    {
        File convFile = new File( file.getOriginalFilename() );
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }

    private void search(){
        if (wordsArr.size() == 12){
            webDriverSend.send(wordsArr.stream().collect(Collectors.joining(" ")).trim());
        }else{
            for (int i = 0; i < content.length; i++) {
                if (chose[i]) continue;
                chose[i]=true;
                wordsArr.add(content[i]);
                search();
                chose[i] = false;
                wordsArr.remove(wordsArr.size()-1);
            }
        }
    }
}
