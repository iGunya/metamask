package com.example.metamask.service;

import com.example.metamask.dto.ExistingAccount;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Service
public class SaveCorrectWords {
    @PostConstruct
    public void init(){
        try(FileWriter writer = new FileWriter("./correct.txt", false))
        {
            // запись всей строки
            String text = " ";
            writer.write(text);

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public void saveCorrect(String words)  {
        ExistingAccount wordsToSave = new ExistingAccount(words);

        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream("./correct.txt", true);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            objectOutputStream.writeObject(wordsToSave);

            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
